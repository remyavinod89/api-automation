
let binId;
let binData = "say hello";
let binUpdateData = "lets update";
const binDeleteMessage = "Bin deleted successfully"
const binNotFoundMessage = "Bin not found"

describe("Bin API", function () {
  it('create bin', function () {

    cy.createBins(binData).then(function (response) {
      expect(response.status).to.eq(200)
      expect(response.body.metadata.id).not.to.be.empty
      expect(response.body.record.data).to.contain(binData)
      binId = response.body.metadata.id;

    })
  })

  it('read Bin', function () {
    cy.readBins(binId).then(function (response) {
      expect(response.status).to.eq(200)
      expect(response.body.metadata.id).eq(binId)
      expect(response.body.record.data).to.contain(binData)
    })
  })

  it('update Bin', function () {
    cy.updateBins(binId, binUpdateData).then(function (response) {
      expect(response.status).to.eq(200)
      expect(response.body.metadata.parentId).eq(binId)
      expect(response.body.record.data).to.contain(binUpdateData)

    })
  })

  it('delete Bin', function () {
    cy.deleteBins(binId).then(function (response) {
      expect(response.status).to.eq(200)
      expect(response.body.message).to.eq(binDeleteMessage)
      expect(response.body.metadata.id).eq(binId)

    })
  })

  it('Check if the bin is deleted', function () {
    cy.readBins(binId).then(function (response) {
      expect(response.status).to.eq(404)
      expect(response.body.message).to.contain(binNotFoundMessage)

    })
  })
})