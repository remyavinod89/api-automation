///<reference types="cypress"/>

let binsURL = Cypress.env('bins_url');
let masterKey = Cypress.env('master_key');
let binIdAttempt;


// POST requests for craete bin
Cypress.Commands.add("createBins", function (binData) {
    let requestBody = { "data": binData }
    cy.request({
        method: "POST",
        url: `${binsURL}/b`,
        headers: {
            "Content-Type": "application/json",
            "X-Master-Key": masterKey
        },
        body: requestBody
    
    });
});


//GET request for read bins
Cypress.Commands.add("readBins", function (binId) {
    cy.request({
        method: "GET",
        url: `${binsURL}/b/${binId}`,
        headers: {
            "Content-Type": "application/json",
            "X-Master-Key": masterKey
        },
        failOnStatusCode: false
    })
});


//PUT request for update bins
Cypress.Commands.add("updateBins", function (binId, binUpdateData) {
    let requestBody = { "data": binUpdateData }
    console.log(binId)
    cy.request({
        method: "PUT",
        url: `https://api.jsonbin.io/v3/b/${binId}`,
        headers: {
            "Content-Type": "application/json",
            "X-Master-Key": masterKey
        },
        body: requestBody,
        failOnStatusCode: false
    })
});

//DELETE request for delete bins
Cypress.Commands.add("deleteBins", function (binId) {
    cy.request({
        method: "DELETE",
        url: `https://api.jsonbin.io/v3/b/${binId}`,
        headers:
        {
            "X-Master-Key": masterKey
        },
        failOnStatusCode: false
    })
});








