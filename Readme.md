The test suite includes tests that cover the CRUD operations of jsonbin API. The API tests are automated using Cypress tool with Java script.



# First time running these tests locally

### Step 1: 
Please, follow the steps in [Cypress documentation](https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements)

## How to run the tests

**Please, note:** The commands below,

To run the tests in the IDE, run the following command:
```sh
$ npx cypress open 
``` 
To run the tests "headless", run the following command:
```sh
$ npx cypress run 


##The things I like to add if I have time,

Because it is an API testing I wanted to add schema valications.

schema validation : Schemas are a special, object-based way of defining validations or sanitizations on requests.
At the root-level, you specify field paths as keys, and objects as values -- which define the error messages, locations and validations/sanitizations.

